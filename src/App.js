import React from 'react';
import './App.css';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Home from "./Home/index.jsx";

function App() {
  return (
    <BrowserRouter>
    <Switch>
      <Route exact path = "/" component={Home}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
