import React from "react";
import Axios from "axios";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Button,Input, Grid, CircularProgress } from "@material-ui/core";
import Modal from 'react-modal';
import "./style.scss";
import ReactDOM from 'react-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faSortUp, faSortDown } from '@fortawesome/fontawesome-free-solid';

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    data : [],
    loading: false,
    message: "",
    page: 1,
    perPage: 3,
    modalItem: "",
    show: false,
    modalfirstName: "",
    modallastName: "",
    modalEmail: "",
    total: 1,
    customStyles : {
        content : {
          top                   : '50%',
          left                  : '50%',
          right                 : 'auto',
          bottom                : 'auto',
          marginRight           : '-50%',
          transform             : 'translate(-50%, -50%)'
        }
      },
    };

    this.getData = this.getData.bind(this);
    this.goPrevious = this.goPrevious.bind(this);
    this.goNext = this.goNext.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.sortDataAsc = this.sortDataAsc.bind(this);
    this.sortDataDesc = this.sortDataDesc.bind(this);
    this.compareByAsc = this.compareByAsc.bind(this);
    this.compareByDesc = this.compareByDesc.bind(this);
    this.replaceModal = this.replaceModal.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  async goPrevious() {
      let page = this.state.page;
      if(page > 1){
      page = page - 1;
      this.setState({
        page: page
      });
      this.getData(page);
      }
  }

  async goNext() {
    let page = this.state.page;
    let total = this.state.total;
    if(page < total){
    page = page + 1;
    this.setState({
      page: page
    });
    this.getData(page);
    }
}

replaceModal(index) {
    let data = this.state.data[index];
    console.log("index", index);
    console.log("data", data);
this.setState({
    modalItem: data.id,
    show: true,
    modalfirstName: data.first_name,
    modallastName: data.last_name,
    modalEmail: data.email,
})
}

handleClose() {
    this.setState({
        show: false
    })
}

compareByDesc() {
    return function (a, b) {
      if (a['id'] > b['id']) return -1;
      if (a['id'] < b['id']) return 1;
      return 0;
    };
  }



sortDataDesc() {
    let arrayCopy = [...this.state.data];
    arrayCopy.sort(this.compareByDesc());
    this.setState({data: arrayCopy});
}

compareByAsc() {
    return function (a, b) {
      if (a['id'] < b['id']) return -1;
      if (a['id'] > b['id']) return 1;
      return 0;
    };
  }



sortDataAsc() {
    let arrayCopy = [...this.state.data];
    arrayCopy.sort(this.compareByAsc());
    this.setState({data: arrayCopy});
}

handleChange(e) {
let currentList = [];
let newList = [];

if (e.target.value !== "") {
  currentList = this.state.data;
    
  newList = currentList.filter(item => {
    const lc = item.email.toLowerCase();
    const filter = e.target.value.toLowerCase();
    return lc.includes(filter);
  });
this.setState({
  data: newList
});
}
else {
    this.getData(this.state.page);
}
}

  async getData(page) {
    this.setState({
      loading: true
    });

    try {
        let res = await Axios.get(
          `https://reqres.in/api/users?page=${page}`
        );
      if (res.data != null) {
        console.log("Res Data:", res.data);
        this.setState({
          data: res.data.data,
          total: res.data.total_pages
        });
      } else {
        this.setState({
          message: "No data found."
        });
      }
    } catch (error) {
      this.setState({
        message: "Error loading the data"
      });
    }

    this.setState({
      loading: false
    });
  }

  componentDidMount(){
      this.getData(0);
  }

  render() {
    return (
        <>
        <Paper className="home">
            <input type="text" className="input" onChange={this.handleChange} placeholder="Search..." />
              {this.state.loading && (
            <center>
              <CircularProgress />
            </center>
          )}
          {!this.state.loading && (
        <Table className="content"> 
          <TableHead>
            <TableRow>
              <TableCell align="center">User Id<div className="arrow-container"><FontAwesomeIcon className="up-vote" icon={faSortUp} onClick={() => {
              this.sortDataAsc();
              }}/><FontAwesomeIcon className="down-vote" icon={faSortDown} onClick={() => {
                this.sortDataDesc();
                }}/></div></TableCell>
              <TableCell align="center">First Name</TableCell>
              <TableCell align="center">Last Name</TableCell>
              <TableCell align="center">Email</TableCell>
              <TableCell align="center">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.data.map((row, index) => (
              <TableRow key={row.id}>
                <TableCell align="center">{row.id}</TableCell>
                <TableCell align="center">{row.first_name}</TableCell>
                <TableCell align="center">{row.last_name}</TableCell>
                <TableCell align="center">{row.email}</TableCell>
                <TableCell align="center">
                <Button className="btn btn-primary" data-toggle="modal" data-target="#viewModal" onClick={() => {
                    this.replaceModal(index)
                }}>View</Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
          <div className="pagination">
            {this.state.page != 1 && <div className="pre" onClick={() => {
                          this.goPrevious();
                        }}>(Previous)</div>}
        {this.state.page != this.state.total && <div className="next" onClick={() => {
                          this.goNext();
                        }}>(Next)</div>}
        
        </div>
        </Table>
        )}

      </Paper>
      <Modal isOpen={this.state.show} onRequestClose={this.handleClose} style={this.state.customStyles}>  
      <div className="private-content">
          <p>User Id</p>
          <Input
            placeholder="user id"
            value={this.state.modalItem}
            className="input-tags"
          />
          </div>
      <div className="private-content">
          <p>First Name</p>
          <Input
            placeholder="first name"
            value={this.state.modalfirstName}
            className="input-tags"
          />
        </div>
        <div className="private-content">
          <p>Last Name</p>
          <Input
            placeholder="last name"
            value={this.state.modallastName}
            className="input-tags"
          />
        </div>
        <div className="private-content">
          <p>E-mail ID</p>
          <Input
            placeholder="Email"
            value={this.state.modalEmail}
            className="input-tags"
          />
        </div>
    </Modal>
    </>
    );
  }
}

export default(Home);
